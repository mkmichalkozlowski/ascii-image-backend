package com.example.demo.converter

import junit.framework.Assert.assertEquals
import org.junit.Ignore
import org.junit.Test
import java.awt.image.BufferedImage
import java.awt.image.BufferedImage.TYPE_INT_ARGB
import java.io.File

class ImageConverterTest {

    @Test
    @Ignore
    fun `Convert image to ascii test`() {
        val imageConverter = ImageConverter()

        val bufferedImage = BufferedImage(100, 100, TYPE_INT_ARGB)

        val convertedImage = imageConverter.convertImage(bufferedImage)

        assertEquals("Image is wrongly converted",
                listOf("##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########"),
                convertedImage)
    }

    @Test
    @Ignore
    fun `Convert image to ascii test 2`() {
        val imageConverter = ImageConverter()

        val bufferedImage = BufferedImage(105, 105, TYPE_INT_ARGB)

        val convertedImage = imageConverter.convertImage(bufferedImage)

        assertEquals("Image is wrongly converted",
                listOf("##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########",
                        "##########"),
                convertedImage)
    }

    @Test
    fun `Convert image ugly dont do this at home`() {
        val imageConverter = ImageConverter()
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test.png"))

        val convertImage = imageConverter.convertImage(readImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output.txt").writeText(convertImage.flatMap { listOf(it.map { it.character.toString() }.reduce{a, b -> a + b}) }.reduce{a, b -> a + "\n"+ b})
    }

    @Test
    fun `Convert image ugly dont do this at home too`() {
        val imageConverter = ImageConverter(partWidth = 5, partHeight = 10)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test2.jpg"))

        val convertImage = imageConverter.convertImage(readImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output2.txt").writeText(convertImage.flatMap { listOf(it.map { it.character.toString() }.reduce{a, b -> a + b}) }.reduce{a, b -> a + "\n"+ b})
    }

    @Test
    fun `Convert image ugly dont do this at home three`() {
        val imageConverter = ImageConverter(partWidth = 8, partHeight = 16)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test3.jpg"))

        val convertImage = imageConverter.convertImage(readImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output3.txt").writeText(convertImage.flatMap { listOf(it.map { it.character.toString() }.reduce{a, b -> a + b}) }.reduce{a, b -> a + "\n"+ b})
    }

    @Test
    fun `Convert image ugly dont do this at home four`() {
        val imageConverter = ImageConverter(partWidth = 20, partHeight = 35)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test4.jpg"))

        val convertImage = imageConverter.convertImage(readImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output4.txt").writeText(convertImage.flatMap { listOf(it.map { it.character.toString() }.reduce{a, b -> a + b}) }.reduce{a, b -> a + "\n"+ b})
    }

    @Test
    fun `Convert image ugly dont do this at home five`() {
        val imageConverter = ImageConverter(partWidth = 10, partHeight = 10)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test5.jpg"))

        val convertImage = imageConverter.convertImage(readImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output5.txt").writeText(convertImage.flatMap { listOf(it.map { it.character.toString() }.reduce{a, b -> a + b}) }.reduce{a, b -> a + "\n"+ b})
    }

    @Test
    fun `Convert image ugly dont do this at home six red`() {
        val imageConverter = ImageConverter(partWidth = 10, partHeight = 10)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test6.jpg"))

        val convertImage = imageConverter.convertImage(readImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output6.txt").writeText(convertImage.flatMap { listOf(it.map { it.character.toString() }.reduce{a, b -> a + b}) }.reduce{a, b -> a + "\n"+ b})
    }

    @Test
    fun `Convert image ugly dont do this at home elon`() {
        val imageConverter = ImageConverter(partWidth = 8, partHeight = 15)
//        val imageConverter = ImageConverter(partWidth = 5, partHeight = 8)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("elon.jpg"))

        val convertImage = imageConverter.convertImage(readImage)

        val asHtml = imageConverter.asHtml(convertImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output7.html").writeText(asHtml)
    }

    @Test
    fun `Convert image ugly dont do this at home wave`() {
        val imageConverter = ImageConverter(partWidth = 8, partHeight = 15)
//        val imageConverter = ImageConverter(partWidth = 5, partHeight = 8)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test3.jpg"))

        val convertImage = imageConverter.convertImage(readImage)

        val asHtml = imageConverter.asHtml(convertImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\output3.html").writeText(asHtml)
    }

    @Test
    fun `Convert image ugly dont do this at home grayscale`() {
        val imageConverter = ImageConverter(partWidth = 8, partHeight = 15)
//        val imageConverter = ImageConverter(partWidth = 5, partHeight = 8)
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("grayscale.PNG"))

        val convertImage = imageConverter.convertImage(readImage)

        val asHtml = imageConverter.asHtml(convertImage)

        File("C:\\Users\\kozlowski\\Desktop\\Documentations\\outputG.html").writeText(asHtml)
    }

    @Test
    fun `Test image load from input stream`() {
        val imageConverter = ImageConverter()
        val readImage = imageConverter.readImage(this.javaClass.getResourceAsStream("test.png"))

        assertEquals("Loaded image is different", 745, readImage.width)
        assertEquals("Loaded image is different", 543, readImage.height)
    }

    @Test
    fun `Test image load from file name`() {
        val imageConverter = ImageConverter()
        val readImage = imageConverter.readImage(this.javaClass.getResource("test.png").file)

        assertEquals("Loaded image is different", 745, readImage.width)
        assertEquals("Loaded image is different", 543, readImage.height)
    }

    @Test
    fun `Test color conversion`() {
        val imageConverter = ImageConverter()
        assertEquals("Expected color is different", 255, imageConverter.convertARGBToLuminosity(0xFFFFFF))
        assertEquals("Expected color is different", 0, imageConverter.convertARGBToLuminosity(0))
        assertEquals("Expected color is different", 18, imageConverter.convertARGBToLuminosity(0xFF))
        assertEquals("Expected color is different", 182, imageConverter.convertARGBToLuminosity(0xFF00))
        assertEquals("Expected color is different", 54, imageConverter.convertARGBToLuminosity(0xFF0000))
    }

    @Test
    fun `Convert to character test`() {
        val imageConverter = ImageConverter()
        assertEquals("Expected character is different", ' ', imageConverter.convertToCharacter(255))
        assertEquals("Expected character is different", 'X', imageConverter.convertToCharacter(111))
        assertEquals("Expected character is different", '#', imageConverter.convertToCharacter(0))
    }
}
