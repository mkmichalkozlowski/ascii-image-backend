package com.example.demo

import com.example.demo.converter.FileStorageService
import com.example.demo.converter.ImageConverter
import com.example.demo.image.ImageAscii
import com.example.demo.image.ImageRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import java.lang.RuntimeException
import java.time.LocalDate

@Component
class DatabaseLoader @Autowired
constructor(private val imageRepository: ImageRepository, private val fileService: FileStorageService) : CommandLineRunner {

    @Throws(Exception::class)
    override fun run(vararg args: String) {
//        val imageConverter = ImageConverter(partWidth = 8, partHeight = 15)
//        val lists = imageConverter.convertImage(imageConverter.readImage(fileService.loadFileAsResource("test.png").inputStream))
//        imageRepository.save(ImageAscii(LocalDate.now(), "Title1", "test.png",
//                imageConverter.asTextLines(lists), lists
//        ))
//
//        val lists1 = imageConverter.convertImage(imageConverter.readImage(fileService.loadFileAsResource("elon.jpg").inputStream))
//        imageRepository.save(ImageAscii(LocalDate.now(), "Title2", "elon.jpg",
//                imageConverter.asTextLines(lists1), lists1
//        ))
//
//        val lists2 = imageConverter.convertImage(imageConverter.readImage(fileService.loadFileAsResource("test3.jpg").inputStream))
//        imageRepository.save(ImageAscii(LocalDate.now(), "Title3", "test3.jpg",
//                imageConverter.asTextLines(lists2), lists2
//        ))
    }
}
