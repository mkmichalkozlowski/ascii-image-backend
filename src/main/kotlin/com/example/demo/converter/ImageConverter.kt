package com.example.demo.converter

import java.awt.Rectangle
import java.awt.image.BufferedImage
import java.io.File
import java.io.InputStream
import javax.imageio.ImageIO

class ImageConverter(private val partWidth:Int = 10, private val partHeight:Int = 10) {
    fun readImage(fileName: String): BufferedImage = ImageIO.read(File(fileName))

    fun readImage(inputStream: InputStream): BufferedImage = ImageIO.read(inputStream)

    fun convertImage(image: BufferedImage): List<List<ColoredCharacter>> {

        val result = mutableListOf<List<ColoredCharacter>>()

        for (posY in 0 until image.height  step partHeight) {
            val line = mutableListOf<ColoredCharacter>()

            if (posY + partHeight > image.height) {
                break
            }

            for (posX in 0 until image.width step partWidth) {

                if (posX + partWidth > image.width) {
                    break
                }

                val raster = image.getData(Rectangle(posX, posY, partWidth, partHeight))
                val emptyArray: IntArray? = null
                val intArray = raster.getPixels(posX, posY, partWidth, partHeight, emptyArray)
                val (colorMean, mean) = calculateMean(intArray)
                line.add(ColoredCharacter(convertToCharacter(mean), colorMean))
            }
            result.add(line)
        }

        return result
    }

    private fun calculateMean(array: IntArray): MeanResult {
        var red = 0
        var green = 0
        var blue = 0
        var number = 0
        var sum = 0
        val alpha = 255

        for (cursor in 0 until array.size - 1 step 3) {

            sum += convertARGBToLuminosity(array[cursor], array[cursor + 1], array[cursor + 2])

            red += array[cursor]
            green += array[cursor + 1]
            blue += array[cursor + 2]
            number++
        }

        red /= number
        green /= number
        blue /= number
        sum /= number

        val colorMean = (alpha shl 24) or (red shl 16) or (green shl 8) or blue

        return MeanResult(colorMean, sum)
    }

    fun asHtml(lists: List<List<ColoredCharacter>>): String {
        var result = "<tt>"

        for (innerList in lists) {

            for (character in innerList) {

                val r = character.color shr 16 and 0xff
                val g = character.color shr 8 and 0xff
                val b = character.color and 0xff

                result += "<span style=\"color: rgb($r,$g,$b);\">${character.character}</span>"
            }
            result += "</br>"
        }

        result += "</tt>"
        return result
    }

    fun asTextLines(lists: List<List<ColoredCharacter>>): List<String> {
        val result = mutableListOf<String>()

        for (innerList in lists) {

            var temp = ""
            for (character in innerList) {
                temp += character.character
            }
            result.add(temp)
        }

        return result
    }

    fun convertARGBToLuminosity(argbValue: Int): Int {

        val r = argbValue shr 16 and 0xff
        val g = argbValue shr 8 and 0xff
        val b = argbValue and 0xff

        if (r == g  && g == b) {
            return r
        }

        return (0.2126 * r + 0.7152 * g + 0.0722 * b).toInt()
    }

    fun convertARGBToLuminosity(r: Int, g: Int, b: Int): Int {

        if (r == g  && g == b) {
            return r
        }

        return (0.2126 * r + 0.7152 * g + 0.0722 * b).toInt()
    }

    private val characters = "#\$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI,\"^`'."

    fun convertToCharacter(value: Int): Char {
        return characters.elementAt(((value / 255.0) * (characters.length - 1)).toInt())
    }
}

data class MeanResult(val colorMean: Int, val luminosityMean: Int)

data class ColoredCharacter(val character: Char, val color: Int) {
    override fun toString(): String {
        return "$character;$color"
    }
}
