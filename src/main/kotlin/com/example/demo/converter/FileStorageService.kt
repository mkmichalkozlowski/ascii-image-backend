package com.example.demo.converter

import org.springframework.stereotype.Service
import org.springframework.util.StringUtils
import java.io.IOException
import java.nio.file.StandardCopyOption
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.net.MalformedURLException
import org.springframework.core.io.UrlResource
import org.springframework.core.io.Resource;



@Service
class FileStorageService {

    private val fileStorageLocation: Path = Paths.get("C:\\images")

    fun storeFile(file: MultipartFile): String {
        // Normalize file name
        val fileName = StringUtils.cleanPath(file.originalFilename!!)

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
                throw RuntimeException("Sorry! Filename contains invalid path sequence $fileName")
            }

            // Copy file to the target location (Replacing existing file with the same name)
            val targetLocation = fileStorageLocation.resolve(fileName)
            Files.copy(file.inputStream, targetLocation, StandardCopyOption.REPLACE_EXISTING)

            return fileName
        } catch (ex: IOException) {
            throw RuntimeException("Could not store file $fileName. Please try again!", ex)
        }
    }

    fun loadFileAsResource(fileName: String): Resource {
        try {
            val filePath = this.fileStorageLocation.resolve(fileName).normalize()
            val resource = UrlResource(filePath.toUri())
            return if (resource.exists()) {
                resource
            } else {
                throw RuntimeException("File not found $fileName")
            }
        } catch (ex: MalformedURLException) {
            throw RuntimeException("File not found $fileName", ex)
        }

    }

}
