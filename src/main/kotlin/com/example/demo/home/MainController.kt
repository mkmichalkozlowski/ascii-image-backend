package com.example.demo.home

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.util.*

@RestController
class MainController {
    @GetMapping("/")
    fun returnEmpty(): Nothing = throw PathNotFoundException("Path `/` is not available!")
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class PathNotFoundException(override val message: String?) : Exception(message)

@ControllerAdvice
class CustomizedResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [(PathNotFoundException::class)])
    fun handleUserNotFoundException(ex: PathNotFoundException, request: WebRequest): ResponseEntity<ErrorDetails> {
        val errorDetails = ErrorDetails(Date(), ex.message!!, request.getDescription(false))
        return ResponseEntity(errorDetails, HttpStatus.NOT_FOUND)
    }
}

public class ErrorDetails(
        val timestamp: Date,
        val message: String,
        val details: String)
