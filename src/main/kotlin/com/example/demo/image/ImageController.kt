package com.example.demo.image

import com.example.demo.converter.FileStorageService
import com.example.demo.converter.ImageConverter
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.util.*
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.http.HttpHeaders
import java.lang.RuntimeException
import java.time.LocalDate


@RestController
@CrossOrigin
@RequestMapping("/api/image")
class ImageController(private val examinationRepository: ImageRepository, private val fileService: FileStorageService) {

    @GetMapping("/")
    fun findAll() = examinationRepository.findAll().map { it.toDtoLite() }

    @GetMapping("/{id}")
    fun findById(@PathVariable id: UUID): ImageAsciiDto {
        val findById = examinationRepository.findById(id)

        if (findById.isPresent) {
            return findById.get().toDto()
        } else {
            throw RuntimeException()
        }
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: UUID): ResponseEntity<Any> {
        if (examinationRepository.existsById(id)) {
            examinationRepository.deleteById(id)
        } else {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }

        return ResponseEntity(HttpStatus.OK)
    }

    @GetMapping("/{id}/image")
    @ResponseBody
    fun findThumbnail(@PathVariable id: UUID): ResponseEntity<Any> {

        val asciiImage = examinationRepository.findById(id).orElseThrow { RuntimeException() }

        val headers = HttpHeaders()
        headers.set("Content-Type", "image/jpeg")
        val resource = fileService.loadFileAsResource(asciiImage.filename)
        return ResponseEntity(resource, headers, HttpStatus.OK)
    }


    @PostMapping("/add")
    fun addNewImage(@RequestParam("file") file: MultipartFile, @RequestParam("title") title: String): ResponseEntity<Any>  {
        try {
            val filename = fileService.storeFile(file)

            val imageConverter = ImageConverter(partWidth = 8, partHeight = 15)
            val bufferedImage = imageConverter.readImage(fileService.loadFileAsResource(filename).inputStream)
            val convertedImage = imageConverter.convertImage(bufferedImage)
            val asTextLines = imageConverter.asTextLines(convertedImage)
            val imageAscii = ImageAscii(LocalDate.now(), title, filename, asTextLines, convertedImage)

            examinationRepository.save(imageAscii)
        } catch(ex: Exception) {
            return ResponseEntity(HttpStatus.BAD_REQUEST)
        }

        return ResponseEntity(HttpStatus.ACCEPTED)
    }
}
