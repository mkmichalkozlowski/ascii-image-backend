package com.example.demo.image

import com.example.demo.converter.ColoredCharacter
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import javax.persistence.AttributeConverter

@Entity
class ImageAscii (
        var date: LocalDate = LocalDate.MIN,

        var title: String = "",

        val filename: String = "",

        @Convert(converter = StringListConverter::class)
        @Column(columnDefinition="TEXT")
        var text: List<String> = emptyList(),

        @Convert(converter = StringColorListConverter::class)
        @Column(columnDefinition="TEXT")
        var coloredText: List<List<ColoredCharacter>> = emptyList(),

        @Id
        @GeneratedValue
        var id: UUID? = null) {

    fun toDto(): ImageAsciiDto = ImageAsciiDto(title, date, text, coloredText, id!!)

    fun toDtoLite(): ImageAsciiDto = ImageAsciiDto(title, date, emptyList(), emptyList(), id!!)
}


data class ImageAsciiDto(val title: String, val date: LocalDate, val text: List<String>, val coloredText: List<List<ColoredCharacter>>, val id: UUID)

@Converter
class StringListConverter : AttributeConverter<List<String>, String> {

    override fun convertToDatabaseColumn(stringList: List<String>): String {
        return stringList.joinToString(SPLIT_CHAR)
    }

    override fun convertToEntityAttribute(string: String): List<String> {
        return string.split(SPLIT_CHAR)
    }

    companion object {
        private val SPLIT_CHAR = ";"
    }
}

@Converter
class StringColorListConverter : AttributeConverter<List<List<ColoredCharacter>>, String> {

    override fun convertToDatabaseColumn(stringList: List<List<ColoredCharacter>>): String {
        val result = mutableListOf<String>()
        for (row in stringList) {
            val joinedRow = row.joinToString(SPLIT_CHARS)
            result.add(joinedRow)
        }

        return result.joinToString(SPLIT_ROWS)
    }

    override fun convertToEntityAttribute(string: String):List<List<ColoredCharacter>> {

        val result = mutableListOf<List<ColoredCharacter>>()

        val splitRows = string.split(SPLIT_ROWS)

        for (row in splitRows) {
            val rowResult = mutableListOf<ColoredCharacter>()
            val split = row.split(SPLIT_CHARS)
            for (x in split.indices step 2) {
                rowResult.add(ColoredCharacter(split[x].toCharArray()[0], split[x + 1].toInt()))
            }
            result.add(rowResult)
        }

        return result
    }

    companion object {
        private val SPLIT_CHARS = ";"
        private val SPLIT_ROWS = ":"
    }
}
